import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookComponent } from './components/book/book.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/auth/login/login.component';

const routes: Routes = [
  { path: 'books', component: BookComponent },
  { path: 'users', component: UserComponent },
  { path: 'admin', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
