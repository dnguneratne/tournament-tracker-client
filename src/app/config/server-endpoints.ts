const baseServerURL = 'api/';

export const serverEndPoint = {
  bookURL: baseServerURL + 'book',
  loginURL: baseServerURL + 'login',
};
